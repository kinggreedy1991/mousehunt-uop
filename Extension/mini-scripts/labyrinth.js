function prorityLength(choice) {
    if (endWith(choice, "s")) return 1;
    if (endWith(choice, "m")) return 2;
    if (endWith(choice, "l")) return 3;

    return 0;
}

function prorityHall(name) {

    var bestHall = 1;
    if (name.indexOf('Plain') > -1) return 1; /*stop running and return*/

    if (name.indexOf('Superior') > -1) bestHall = 2;
    if (name.indexOf('Epic') > -1) bestHall = 5;

    if (name.indexOf('Short') > -1) bestHall = bestHall * 1;
    if (name.indexOf('Medium') > -1) bestHall = bestHall * 2;
    if (name.indexOf('Long') > -1) bestHall = bestHall * 3;

    return bestHall;
}

function hallIsBest(doors) {
    var result = "";
    var hallCurrent = 1;

    for (var i = 0; i < doors.length; i++) {
        var choice = doors[i].choice;
        if (choice == null) continue;
        var name = doors[i].name;

        if (prorityHall(name) > hallCurrent) {
            result = choice;
            hallCurrent = prorityHall(name);
        }
    }

    return result;
}

function longIsBest(doors, type) {
    var result = "";
    var leCurrent = 0;

    for (var i = 0; i < doors.length; i++) {
        var choice = doors[i].choice;
        if (choice == null) continue;

        if (startWith(choice, type) && prorityLength(choice) > leCurrent) {
            result = choice;
            leCurrent = prorityLength(choice);
        }
    }

    return result;
}

    function shortIsBest(doors, type) {
    var result = "";
    var leCurrent = 99;

    for (var i = 0; i < doors.length; i++) {
        var choice = doors[i].choice;
        if (choice == null) continue;

        if (startWith(choice, type) && prorityLength(choice) < leCurrent) {
            result = choice;
            leCurrent = prorityLength(choice);
        }
    }

    return result;
}

    function defaultShortDoor(doors) {
    var result = "";
    var leCurrent = 99;

    for (var i = 0; i < doors.length; i++) {
        var choice = doors[i].choice;
        if (choice == null) continue;

        if (result == "" || prorityLength(choice) < leCurrent) {
            result = choice;
            leCurrent = prorityLength(choice);
        }
    }

    return result;
}

function priorityDoor(doors, type, le) {

    //priority hall
    var hall = hallIsBest(doors);
    if (hall.length > 0) return hall;

    //priory match
    if (le == 'l') {
        var matchChoice = longIsBest(doors, type);
        if (matchChoice.length > 0) return matchChoice;
    }
    else {
        var matchChoice = shortIsBest(doors, type);
        if (matchChoice.length > 0) return matchChoice;
    }

    return defaultShortDoor(doors);
}

function enterLabyrinth() {
    if (data.user.environment_id == 51) {
        if (data.user.bait_quantity <= 3) {
            if (data.user.bait_item_id != 98) {
                setTimeout(function () { shLoadOnce(C_shdefaultAction.CHANGETRAP, shChangeTrap('', '', '', 'gouda_cheese'), autoPlay); }, 5000);
            }
            if (data.user.bait_item_id == 98) {
                console.log('buy 300 gouda_cheese');
                setTimeout(function () { shLoadOnce(C_shdefaultAction.PURCHASE, shPurchase("buy", "gouda_cheese", 300), null); }, 5000);
            }
        }
    }

    if (data.user.environment_id == 52) {        
        var labyObj = data.user.quests.QuestLabyrinth;

        var priority = sh_scripts[sh_sid].vars.autoEnterDoorWhenEnterLaby.val;        

        var autoLantern = labyObj.total_clues >= sh_scripts[sh_sid].vars.turnOnLantern.val;
        var autoArmGG = labyObj.total_clues >= sh_scripts[sh_sid].vars.armGG.val;
        var autoChooseDoor = labyObj.total_clues <= sh_scripts[sh_sid].vars.autoChooseDoor.val;
        var armCharm = labyObj.total_clues >= sh_scripts[sh_sid].vars.armCharm.val;

        if (data.user.bait_quantity <= 3) {
            if (data.user.bait_item_id != 98) {
                setTimeout(function () { shLoadOnce(C_shdefaultAction.CHANGETRAP, shChangeTrap('', '', '', 'gouda_cheese'), autoPlay); }, 5000);
            }
            if (data.user.bait_item_id == 98) {
                console.log('buy 300 gouda_cheese');
                setTimeout(function () { shLoadOnce(C_shdefaultAction.PURCHASE, shPurchase("buy", "gouda_cheese", 300), null); }, 5000);
            }
        }
        
        if (autoLantern && labyObj.items.labyrinth_lantern_fuel <= 2) {
            var minPlate = labyObj.items.plate_of_fealty_crafting_item;
            var minTech = labyObj.items.tech_power_core_crafting_item;
            var minScholar = labyObj.items.ancient_scholar_scroll_crafting_item;

            var craftQty = (10, minPlate, minTech, minScholar)
            if (craftQty > 0)
                shLoadOnce(C_shdefaultAction.PURCHASE, shPurchase("buy", "labyrinth_lantern_fuel", craftQty), null);
        }
        
        if (prorityHall(labyObj.hallway_name) >= 2 && autoArmGG) {        
            var arr = [];
            arr.push({ name: "glowing_gruyere_cheese", trinket: "", quantity: labyObj.items.glowing_gruyere_cheese, baitId: 1733 });
            arr.push({ name: "gouda_cheese", trinket: "", quantity: 300, baitId: 98 });
            priorityCheese(data.user.bait_item_id, 2, arr)
            
            if (armCharm != '') {
                setTimeout(function () { shLoadOnce(C_shdefaultAction.CHANGETRAP, shChangeTrap('', '', armCharm, ''), autoPlay); }, 5000);
            }

            if (labyObj.lantern_status == "inactive"
                && labyObj.items.labyrinth_lantern_fuel > 0
                && autoLantern) {
                setTimeout(function () { shLoadOnce("/managers/ajax/environment/labyrinth.php", "&action=toggle_lantern&last_read_journal_entry_id=" + getLastReadJournalEntryId(), null); }, 5000);
            }
        }

        if (labyObj.status == "exit") {
            var maxClue = 0;
            var index = 0;
            for (var i = 0; i < labyObj.all_clues.length - 1 /*not count deadend*/ ; i++) {
                if (labyObj.all_clues[i].quantity > maxClue) {
                    index = i;
                    maxClue = labyObj.all_clues[i].quantity;
                }
            }

            var qchoice = labyObj.doors[index].choice;

            var arr = [];
            arr.push({ name: "glowing_gruyere_cheese", trinket: "", quantity: labyObj.items.glowing_gruyere_cheese, baitId: 1733 });
            arr.push({ name: "gouda_cheese", trinket: "", quantity: 300, baitId: 98 });
            priorityCheese(data.user.bait_item_id, 2, arr)            

            console.log('choice:' + qchoice)
            setTimeout(function () { shLoadOnce("/managers/ajax/environment/labyrinth.php", "&action=make_intersection_choice&choice=" + qchoice + "&last_read_journal_entry_id=" + getLastReadJournalEntryId(), autoPlay); }, 5000);
            return;
        }

        if ((labyObj.status == "intersection" || labyObj.status == 'intersection entrance') && autoChooseDoor) {

            var deadEnd = labyObj.all_clues[5].quantity;
            if (labyObj.can_scramble_doors == "active" && labyObj.total_clues - deadEnd < 3 && priority < 3) {
                console.log('retreat');
                setTimeout(function () { shLoadOnce("/managers/ajax/environment/labyrinth.php", "action=retreat&last_read_journal_entry_id=" + getLastReadJournalEntryId(), autoPlay); }, 5000);
                return;
            }

            var FEALTY = 'y', TECH = 'h', SCHOLAR = 's', TREASURE = 't', FARM = 'f', SHORT = 's', MEDIUM = 'm', LONG = 'l';

            var plateOfFealty = labyObj.items.plate_of_fealty_crafting_item;
            var techPowerCore = labyObj.items.tech_power_core_crafting_item;
            var ancientScholar = labyObj.items.ancient_scholar_scroll_crafting_item;

            var clue = 0;
            var type = "";
            if (priority < 0) {
                var minimunCraftItem = 9999;
                if (plateOfFealty < minimunCraftItem) {
                    priority = 0;
                    minimunCraftItem = plateOfFealty;
                }
                if (techPowerCore < minimunCraftItem) {
                    priority = 1;
                    minimunCraftItem = techPowerCore;
                }
                if (ancientScholar < minimunCraftItem) {
                    priority = 2;
                    minimunCraftItem = ancientScholar;
                }
            }

            console.log('choose type: ' + type);
            type = labyObj.all_clues[priority].type;
            clue = labyObj.all_clues[priority].quantity;

            var doors = labyObj.doors;

            var choice = "";

            if (clue <= 10) choice = priorityDoor(doors, type, LONG);
            if (clue > 10) choice = priorityDoor(doors, type, SHORT);

            if (type == TREASURE && !startWith(choice, type)) {
                type = FARM;
                choice = priorityDoor(doors, type, LONG);
            }
            else if (type == FARM && !startWith(choice, type)) {
                type = TREASURE;
                choice = priorityDoor(doors, type, LONG);
            }

            if (choice.length > 0) {
                
                var arr = [];                
                arr.push({ name: "gouda_cheese", trinket: "", quantity: 300, baitId: 98 });
                priorityCheese(data.user.bait_item_id, 2, arr)
                                
                console.log('Entrance ' + choice);
                setTimeout(function () { shLoadOnce("/managers/ajax/environment/labyrinth.php", "action=make_intersection_choice&choice=" + choice + "&last_read_journal_entry_id=" + getLastReadJournalEntryId(), autoPlay); }, 5000);
            }
        }
    }

    setTimeout(shFunctionSuccessHandler, 0);
}
