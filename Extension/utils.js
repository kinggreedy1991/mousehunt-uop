
function endWith(s, c) {
    return s.substr(s.length - c.length, c.length) == c;
}

function startWith(s, c) {
    return s.substring(0, c.length) == c;
}

function prorityLength(choice) {
    if (endWith(choice, "s")) return 1;
    if (endWith(choice, "m")) return 2;
    if (endWith(choice, "l")) return 3;

    return 0;
}

function priorityCheese(currBaitId, minQty, arr) {
    var prioItem = arr[0]

    for (var i = 0; i < arr.length; i++) {
        var item = arr[i];

        if (item.quantity > minQty && item.baitId == currBaitId) {
            return;
        }

        if (item.quantity > minQty && item.baitId != currBaitId) {
            prioItem = item;
            break;
        }
    }

    setTimeout(function () { shLoadOnce(C_shdefaultAction.CHANGETRAP, shChangeTrap('', '', prioItem.trinket, prioItem.name), autoPlay); }, 5000);
}

function priorityTrinket(currTrinketId, minQty, arr) {
    var prioItem = arr[0];
    for (var i = 0; i < arr.length; i++) {
        var item = arr[i];

        if (item.quantity > minQty && item.trinketId == currTrinketId) {
            return;
        }

        if (item.quantity > minQty && item.trinketId != currTrinketId) {
            prioItem = item;
            break;
        }
    }

    setTimeout(function () { shLoadOnce(C_shdefaultAction.CHANGETRAP, shChangeTrap('', '', prioItem.name, ''), null); }, 5000);
}
